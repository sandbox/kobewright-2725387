<?php
namespace Drupal\search_api_filters\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * A block containing a filter for one field of the Search API index.
 *
 * @Block(
 *   id = "search_api_filter_block",
 *   admin_label = @Translation("Search API Filter block"),
 * )
 */
class FilterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return 'Filter block';
  }

}
